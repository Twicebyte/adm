from typing import Iterable, Union, Optional, Callable, Tuple


class Property:
    pass


class Entity:
    pass


PropertyValueType = Optional[Union[str, int, float, bool]]
EntityChildrenType = Iterable[Union[
    Tuple[str, Iterable[Entity]], Entity,
    Tuple[str, Iterable[Property]], Property,
]]


class P(Property):
    def __init__(
        self, name: str,
        value: PropertyValueType = None,
        canvas: str = '{:0.2f}',
    ):
        self.name = name
        self.value = value
        self.canvas = canvas

    def __repr__(self):
        return '{}: {}'.format(
            self.name,
            self.canvas.format(self.value),
        )

    def describe(self):
        if isinstance(self.value, str):
            return '"{}": "{}"'.format(
                self.name,
                self.value,
            )
        if isinstance(self.value, bool):
            return '"{}": {}'.format(
                self.name,
                'true' if self.value else 'false',
            )
        return '"{}": {}'.format(
            self.name,
            self.value,
        )


class E(Entity):
    def __init__(
        self, name: str,
        children: EntityChildrenType = None,
    ):
        self.name = name
        self.children = dict()
        self.collections = dict()
        for child in children:
            if isinstance(child, tuple):
                name, content = child
                self.collections[name] = content
            else:
                name, content = child.name, child
                self.children[name] = content

    def __getattr__(self, name):
        if name in self.children:
            return self.children[name]
        if name in self.collections:
            return self.collections[name]
        raise KeyError(f'Current entity "{self.name}" have no property "{name}"!')

    def describe(self):
        children_desc = []
        for _, child in self.children.items():
            children_desc.append(child.describe())
        for name, children in self.collections.items():
            collection_children_desc = []
            for child in children:
                collection_children_desc.append(child.describe_content())
            children_desc.append(
                f'"{name}": [{", ".join(collection_children_desc)}]'
            )
        return f'"{self.name}"'+': {'+', '.join(children_desc)+'}'

    def describe_content(self):
        children_desc = []
        for _, child in self.children.items():
            children_desc.append(child.describe())
        for name, children in self.collections.items():
            collection_children_desc = []
            for child in children:
                collection_children_desc.append(child.describe())
            children_desc.append(
                f'"{name}": [{", ".join(collection_children_desc)}]'
            )
        return '{'+', '.join(children_desc)+'}'


CalculusInputType = Iterable[Entity]
CalculusOutputType = Property
CalculusType = Callable[[CalculusInputType], CalculusOutputType]


class Metric:
    def __init__(
        self,
        calculus: CalculusType,
    ):
        self.calculus = calculus

    def __call__(
        self,
        collection: CalculusInputType,
    ) -> CalculusOutputType:
        return self.calculus(collection)


class M(Metric):
    pass
