from typing import List, Union, Optional
from classes_base import E, P, M


class SubscriptionThread(E):
    def __init__(
        self,
        subscription_id: str,
        price: float,
        hits: int = 1,
        is_alive: bool = True,
        is_cancelled: bool = False,
        is_refunded: bool = False,
    ):
        children = [
            P('subscription_id', subscription_id),
            P('price', price, '{:0,.2f} $'),
            P('hits', hits, '{:n} hit(s)'),
            P('is_alive', is_alive),
            P('is_cancelled', is_cancelled),
            P('is_refunded', is_refunded),
        ]
        super().__init__('subscription_thread', children)

    def add_hit(self):
        self.hits.value += 1

    def cancel(self):
        self.is_alive.value = False
        self.is_cancelled.value = True

    def refund(self):
        self.is_alive.value = False
        self.is_cancelled.value = True
        self.is_refunded.value = True


class User(E):
    def __init__(
        self,
        user_id: str,
        media_source: str,
        cost: float,
    ):
        children = [
            P('media_source', media_source),
            P('cost', cost, '{:0,.2f} $'),
            ('subscription_threads', []),
        ]
        super().__init__(user_id, children)

    def add_subscription_thread(self, thread: SubscriptionThread):
        self.subscription_threads.append(thread)

    def get_active_subscription_thread(
        self, subscription_id: str
    ) -> Optional[Union[SubscriptionThread, List[SubscriptionThread]]]:
        candidates = [
            thread
            for thread in self.subscription_threads
            if (
                thread.is_alive.value and
                thread.subscription_id.value == subscription_id
            )
        ]
        return (
            (
                candidates[0]
                if len(candidates) == 1 else
                candidates
            )
            if candidates else
            None
        )


class Metrics:
    users_cost = M(
        lambda users: P(
            'Cost',
            sum(
                [
                    user.cost.value
                    for user in users
                ]
            ),
            '{:0,.2f} $'
        )
    )
    users_revenue = M(
        lambda users: P(
            'Revenue',
            sum(
                [
                    sum([
                        thread.price.value * thread.hits.value
                        for thread in user.subscription_threads
                    ])
                    for user in users
                ]
            ),
            '{:0,.2f} $'
        )
    )
    users_roas = M(
        lambda users: P(
            'ROAS',
            Metrics.users_revenue(users).value /
            Metrics.users_cost(users).value,
            '{:0,.2f}'
        )
    )
